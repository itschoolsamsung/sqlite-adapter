package com.example.al.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity {

	DBHelper dbHelper;
	SQLiteDatabase db;
	final String TABLE_NAME = "person";
	ListView lv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();

		lv = findViewById(R.id.listView);
		lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Toast.makeText(getApplicationContext(), "position: " + i + "\nid: " + l, Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.buttonAdd).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ContentValues cv = new ContentValues();
				cv.put("name", "qwe");
				cv.put("year", 123);
				long index = db.insert(TABLE_NAME, null, cv);
				Log.i("_SQLITE_", "insert id=" + index);
				updateResult();
			}
		});

		findViewById(R.id.buttonClear).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int count = db.delete(TABLE_NAME, null, null);
				Log.i("_SQLITE_", "deleted rows count = " + count);
				updateResult();
			}
		});

		updateResult();
	}

	public void updateResult() {
		ArrayList<String> arr = new ArrayList<>();

		Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);
		if (c.moveToFirst()) {
			int id = c.getColumnIndex("id");
			int name = c.getColumnIndex("name");
			int year = c.getColumnIndex("year");
			do {
				arr.add("id=" + c.getInt(id) + ", name=" + c.getString(name) + ", year=" + c.getInt(year));
				Log.i("_SQLITE_", "id=" + c.getInt(id) + ", name=" + c.getString(name) + ", year=" + c.getInt(year));
			} while (c.moveToNext());
		} else {
			Log.i("_SQLITE_", "0 records");
		}
		c.close();

		ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.list_item, arr);
		lv.setAdapter(adapter);
	}

	class DBHelper extends SQLiteOpenHelper {

		DBHelper(Context context) {
			super(context, "testDB", null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.i("_SQLITE_", "on create");
			db.execSQL(
					"create table " + TABLE_NAME + " (" +
					"id integer primary key autoincrement," +
					"name varchar(100)," +
					"year integer" +
					");"
			);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}
}
